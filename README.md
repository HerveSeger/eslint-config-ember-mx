# eslint-config-ember-mx

This package provides an [ESLint](http://eslint.org/) configuration file (`.eslintrc`) based on the excellent [airbnb  package](https://github.com/airbnb/javascript/tree/master/packages/eslint-config-airbnb).

This ESLint configuration targets Ember applications.

## Usage

###Install with `ember-mx` addon
Run `ember install ember-mx` on a new project to have the file `.eslintrc` created for you and the required dependencies installed.

###Manual install
 

 1. Install the dependencies:
   ```
   npm install eslint-config-airbnb eslint --save-dev
   ```
   
 2. Create a file `.eslintrc` at the root of your project with the following content:
```json
// .eslintrc
{
	"extends": "ember-mx"
}
```

See [Airbnb's Javascript styleguide](https://github.com/airbnb/javascript) and
the [ESlint config docs](http://eslint.org/docs/user-guide/configuring#extending-configuration-files)
for more information.
