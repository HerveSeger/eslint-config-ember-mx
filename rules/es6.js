module.exports = {
	'ecmaFeatures': {
		'jsx': false
	},
	'rules': {
		// 0: never (rule disabled)
		// 1: warning
		// 2: error
		// require method and property shorthand syntax for object literals
		'object-shorthand': [1, 'always'],
		// Require let or const instead of var
		"no-var": 1
	}
};