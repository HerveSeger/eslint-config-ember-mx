module.exports = {
	'rules': {
		// require method and property shorthand syntax for object literals
		'object-shorthand': 0,
		// Require let or const instead of var
		"no-var": 0,
		"prefer-const": 0
	}
};