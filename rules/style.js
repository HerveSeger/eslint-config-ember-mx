module.exports = {
	"rules": {
		// 0: never (rule disabled)
		// 1: warning
		// 2: error
		// Disallow or enforce spaces inside of brackets.
		"array-bracket-spacing": [2, "never"],
		// Require file to end with single newline
		"eol-last": 0,
		// Require Function Expressions to have a Name 
		"func-names": 0,
		// This option validates a specific tab width for your code in block statements.
		"indent": [0, "tab", {
			"SwitchCase": 1,
			"VariableDeclarator": 1
		}],
		// specify the maximum length of a line in your program
		// https://github.com/eslint/eslint/blob/master/docs/rules/max-len.md
		'max-len': [2, 150, 2, {
			'ignoreUrls': true,
			'ignoreComments': true
		}],
		// Require Constructors to Use Initial Caps. Updated to fit Ember shorthand utilities such as Ember.A for Ember.Array
		'new-cap': [2, {
			"capIsNewExceptions": ["Ember.A", "Ember.K"]
		}],
		// Disallow trailing spaces at the end of lines
		"no-trailing-spaces": [2, {
			"skipBlankLines": true
		}],
		// Disallow or enforce spaces inside of curly braces in objects. 
		"object-curly-spacing": [2, "always", {
			"objectsInObjects": false,
			"arraysInObjects": false
		}],
		// Enforce padding within blocks		
		"padded-blocks": 0,
		// Enforce Quote Style
		"quotes": [2, "single"],
		// Quoting Style for Property Names. 
		// The reserved JS keywords must be quoted. Numbers as well.
		"quote-props": [2, "as-needed", {
			"keywords": true,
			"numbers": true,
			"unnecessary": false
		}],
		// Enforce or Disallow Semicolons
		"semi": [2, "always"],
		// No space before function parentheses
		"space-before-function-paren": 0
	}
};