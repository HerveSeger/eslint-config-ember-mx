module.exports = {
	"rules": {
		// 0: never (rule disabled)
		// 1: warning
		// 2: error
		// Error - Disallow or Enforce Dangling Commas
		"comma-dangle": 0,
		// Disallow Empty Block Statements
		"no-empty": 1
	}
};