module.exports = {
	'rules': {
		// Disallow Unused Variables
		// The unused function args do not trigger an error
		'no-unused-vars': [2, {
			"args": "none"
		}]
	}
};