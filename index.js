module.exports = {
	'extends': [
		'eslint-config-airbnb/base',
		'eslint-config-ember-mx/config/setup',
		'eslint-config-ember-mx/config/env',
		'eslint-config-ember-mx/rules/best-practices',
		'eslint-config-ember-mx/rules/style',
		'eslint-config-ember-mx/rules/es6',
		'eslint-config-ember-mx/rules/errors',
		'eslint-config-ember-mx/rules/variables'
	]
};